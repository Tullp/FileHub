package com.teamdev.services;

import com.google.common.base.Preconditions;

/**
 * Base abstraction, that represents "Value object" pattern.
 *
 * @param <T> type of data, that value object wrapping.
 */
public class ValueObject<T> {

	private final T content;

	protected ValueObject(T content) {

		this.content = validate(content);
	}

	public T content() {

		return content;
	}

	private T validate(T content) {

		return Preconditions.checkNotNull(content);
	}
}
