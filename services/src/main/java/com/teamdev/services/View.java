package com.teamdev.services;

import com.teamdev.persistent.user.UserSessionStorage;

/**
 * Base abstraction. Needs for sending data to REST layer.
 *
 * @param <Q> type of {@linkplain Query query}, that a particular view needs.
 * @param <R> type of value, that view must returns.
 */
public abstract class View<Q extends Query, R> extends SecuredProcess<Q, R> {

	protected View(UserSessionStorage userSessionStorage) {

		super(userSessionStorage);
	}
}
