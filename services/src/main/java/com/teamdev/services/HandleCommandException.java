package com.teamdev.services;

/**
 * Throws during commands handling into a processes or views.
 */
public class HandleCommandException extends Exception {

	private static final long serialVersionUID = -1524544122411950114L;

	public HandleCommandException(String message) {

		super(message);
	}
}
