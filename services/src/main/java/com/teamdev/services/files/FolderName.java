package com.teamdev.services.files;

import com.teamdev.services.ValueObject;

/**
 * Implementation of {@link ValueObject}, using in {@link CreateFolderCommand}.
 */
class FolderName extends ValueObject<String> {

	FolderName(String content) {

		super(content);
	}
}
