package com.teamdev.services.files;

/**
 * Throws when user put in command or query invalid folder id.
 */
class InvalidFolderIdException extends RuntimeException {

	private static final long serialVersionUID = 6880357299539492098L;

	InvalidFolderIdException() {

		super("Invalid folder id.");
	}
}
