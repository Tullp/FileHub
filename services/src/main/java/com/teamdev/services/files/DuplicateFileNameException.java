package com.teamdev.services.files;

import com.teamdev.services.HandleCommandException;

/**
 * Throws during attempt of create new file with already existent name.
 */
public class DuplicateFileNameException extends HandleCommandException {

	private static final long serialVersionUID = 4069249434040367309L;

	public DuplicateFileNameException() {

		super("File with this name is already exists.");
	}
}
