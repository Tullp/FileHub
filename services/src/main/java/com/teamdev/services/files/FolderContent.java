package com.teamdev.services.files;

import java.util.List;

/**
 * Data transfer object, that needs for represent information about folder content.
 */
public class FolderContent {

	private final List<FileInfo> files;

	private final List<FolderInfo> folders;

	public FolderContent(List<FileInfo> files, List<FolderInfo> folders) {

		this.files = files;

		this.folders = folders;
	}

	public List<FileInfo> files() {

		return files;
	}

	public List<FolderInfo> folders() {

		return folders;
	}
}
