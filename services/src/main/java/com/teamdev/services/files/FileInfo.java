package com.teamdev.services.files;

import com.google.common.base.Preconditions;

/**
 * Data transfer object, that needs for represent base information about file.
 */
public class FileInfo {

	private final String fileId;

	private final String parentFolderId;

	private final String name;

	private final String mimeType;

	public FileInfo(String fileId, String parentFolderId, String name, String mimeType) {

		this.fileId = Preconditions.checkNotNull(fileId);

		this.parentFolderId = Preconditions.checkNotNull(parentFolderId);

		this.name = Preconditions.checkNotNull(name);

		this.mimeType = Preconditions.checkNotNull(mimeType);
	}

	public String fileId() {

		return fileId;
	}

	public String parentFolderId() {

		return parentFolderId;
	}

	public String name() {

		return name;
	}

	public String mimeType() {

		return mimeType;
	}
}
