package com.teamdev.services.files;

import com.teamdev.persistent.files.FolderId;
import com.teamdev.persistent.user.UserId;
import com.teamdev.services.AuthToken;

/**
 * Builder for {@link CreateFolderCommand}.
 */
public class CreateFileCommandBuilder {

	private AuthToken authToken;

	private String name;

	private String mimeType;

	private FolderId folderId;

	private UserId userId;

	private byte[] content;

	public CreateFileCommand build() {

		return new CreateFileCommand(authToken, name, mimeType, folderId, userId, content);
	}

	public CreateFileCommandBuilder setAuthToken(AuthToken authToken) {

		this.authToken = authToken;

		return this;
	}

	public CreateFileCommandBuilder setName(String name) {

		this.name = name;

		return this;
	}

	public CreateFileCommandBuilder setMimeType(String mimeType) {

		this.mimeType = mimeType;

		return this;
	}

	public CreateFileCommandBuilder setFolderId(FolderId folderId) {

		this.folderId = folderId;

		return this;
	}

	public CreateFileCommandBuilder setUserId(UserId userId) {

		this.userId = userId;

		return this;
	}

	public CreateFileCommandBuilder setContent(byte[] content) {

		this.content = content.clone();

		return this;
	}
}
