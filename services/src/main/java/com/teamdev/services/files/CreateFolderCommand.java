package com.teamdev.services.files;

import com.google.common.base.Preconditions;
import com.teamdev.persistent.files.FolderId;
import com.teamdev.persistent.user.UserId;
import com.teamdev.services.AuthToken;
import com.teamdev.services.AuthenticatedUserCommand;
import com.teamdev.services.user.RegistrationProcess;

/**
 * Implementation of {@link AuthenticatedUserCommand}. Needs for call {@link CreateFolderProcess}.
 */
public class CreateFolderCommand extends AuthenticatedUserCommand {

	private final FolderName name;

	private final UserId userId;

	private final FolderId parentFolderId;

	public CreateFolderCommand(AuthToken authToken, String name, UserId userId, FolderId parentFolderId) {

		super(authToken);

		this.name = new FolderName(name);

		this.userId = Preconditions.checkNotNull(userId);

		this.parentFolderId = Preconditions.checkNotNull(parentFolderId);
	}

	public String name() {

		return name.content();
	}

	public UserId userId() {

		return userId;
	}

	public FolderId parentFolderId() {

		return parentFolderId;
	}
}
