package com.teamdev.services.files;

import com.teamdev.persistent.files.FileStorage;
import com.teamdev.persistent.files.FolderId;
import com.teamdev.persistent.files.FolderStorage;
import com.teamdev.persistent.user.UserSessionStorage;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.View;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of {@link View}. Needs for getting folder content (files and folders).<br>
 * Required {@link FolderContentQuery}.
 */
public class FolderContentView extends View<FolderContentQuery, FolderContent> {

	private final FileStorage fileStorage;

	private final FolderStorage folderStorage;

	public FolderContentView(UserSessionStorage userSessionStorage, FileStorage fileStorage, FolderStorage folderStorage) {

		super(userSessionStorage);

		this.fileStorage = fileStorage;

		this.folderStorage = folderStorage;
	}

	@Override
	protected FolderContent doHandle(FolderContentQuery command) throws HandleCommandException {

		List<FileInfo> files = fileStorage.findByFolderId(new FolderId(command.parentFolderId())).stream()
			.map(file -> new FileInfo(
				file.id().identifier(),
				file.folderId().identifier(),
				file.name(),
				file.mimeType()
			)).collect(Collectors.toList());

		List<FolderInfo> folders = folderStorage.findByParentFolderId(new FolderId(command.parentFolderId())).stream()
			.map(folder -> new FolderInfo(
				folder.id().identifier(),
				folder.name()
			)).collect(Collectors.toList());

		return new FolderContent(files, folders);
	}
}
