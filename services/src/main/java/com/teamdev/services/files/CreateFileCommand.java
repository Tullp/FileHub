package com.teamdev.services.files;

import com.google.common.base.Preconditions;
import com.teamdev.persistent.files.FolderId;
import com.teamdev.persistent.user.UserId;
import com.teamdev.services.AnonymousUserCommand;
import com.teamdev.services.AuthToken;
import com.teamdev.services.AuthenticatedUserCommand;
import com.teamdev.services.user.RegistrationProcess;

/**
 * Implementation of {@link AuthenticatedUserCommand}. Needs for call {@link CreateFileProcess}.
 */
public class CreateFileCommand extends AuthenticatedUserCommand {

	private final FileName name;

	private final FileMimeType mimeType;

	private final FolderId folderId;

	private final UserId userId;

	private final byte[] content;

	public CreateFileCommand(AuthToken authToken, String name, String mimeType, FolderId folderId, UserId userId, byte[] content) {

		super(authToken);

		this.name = new FileName(name);

		this.mimeType = new FileMimeType(mimeType);

		this.folderId = Preconditions.checkNotNull(folderId);

		this.userId = Preconditions.checkNotNull(userId);

		this.content = content.clone();
	}

	public String name() {

		return name.content();
	}

	public String mimeType() {

		return mimeType.content();
	}

	public FolderId folderId() {

		return folderId;
	}

	public UserId userId() {

		return userId;
	}

	public byte[] content() {

		return content.clone();
	}
}
