package com.teamdev.services.files;

import com.teamdev.services.ValueObject;

/**
 * Implementation of {@link ValueObject}, using in {@link CreateFileCommand}.
 */
class FileName extends ValueObject<String> {

	FileName(String content) {

		super(content);
	}
}
