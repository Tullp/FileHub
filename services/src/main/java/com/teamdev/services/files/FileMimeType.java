package com.teamdev.services.files;

import com.teamdev.services.ValueObject;

/**
 * Implementation of {@link ValueObject}, using in {@link CreateFileCommand}.
 */
class FileMimeType extends ValueObject<String> {

	FileMimeType(String content) {

		super(content);
	}
}
