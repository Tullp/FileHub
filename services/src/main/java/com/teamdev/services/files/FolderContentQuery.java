package com.teamdev.services.files;

import com.google.common.base.Preconditions;
import com.teamdev.services.AuthToken;
import com.teamdev.services.Query;

/**
 * Implementation of {@link Query}. Needs for call {@link FolderContentView}.
 */
public class FolderContentQuery extends Query {

	private final String parentFolderId;

	public FolderContentQuery(AuthToken authToken, String parentFolderId) {

		super(authToken);

		this.parentFolderId = Preconditions.checkNotNull(parentFolderId);
	}

	public String parentFolderId() {

		return parentFolderId;
	}
}
