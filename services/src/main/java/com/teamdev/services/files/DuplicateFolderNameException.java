package com.teamdev.services.files;

import com.teamdev.services.HandleCommandException;

/**
 * Throws during attempt of create new folder with already existent name.
 */
public class DuplicateFolderNameException extends HandleCommandException {

	private static final long serialVersionUID = -518448222180588572L;

	public DuplicateFolderNameException() {

		super("Folder with this name is already exists.");
	}
}
