package com.teamdev.services.files;

import com.teamdev.persistent.DuplicatedDataRecordIdException;
import com.teamdev.persistent.files.FileContentId;
import com.teamdev.persistent.files.FileContentRecord;
import com.teamdev.persistent.files.FileContentStorage;
import com.teamdev.persistent.files.FileId;
import com.teamdev.persistent.files.FileRecord;
import com.teamdev.persistent.files.FileStorage;
import com.teamdev.persistent.files.FolderStorage;
import com.teamdev.persistent.user.UserSessionStorage;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.SecuredProcess;

import java.time.Instant;

/**
 * Implementation of {@link SecuredProcess}. Needs for create files.<br>
 * Required {@link CreateFileCommand}.
 */
public class CreateFileProcess extends SecuredProcess<CreateFileCommand, String> {

	private final FileStorage fileStorage;

	private final FileContentStorage fileContentStorage;

	private final FolderStorage folderStorage;

	public CreateFileProcess(UserSessionStorage userSessionStorage, FileStorage fileStorage, FileContentStorage fileContentStorage, FolderStorage folderStorage) {

		super(userSessionStorage);

		this.fileStorage = fileStorage;

		this.fileContentStorage = fileContentStorage;

		this.folderStorage = folderStorage;
	}

	@Override
	protected String doHandle(CreateFileCommand command) throws HandleCommandException {

		if (!folderStorage.findById(command.folderId()).isPresent()) {

			throw new InvalidFolderIdException();
		}

		FileRecord fileRecord = new FileRecord(new FileId(String.format(
			"%s:%s:%d",
			command.folderId().identifier(),
			command.name(),
			Instant.now().getEpochSecond()
		)));

		fileRecord.setFolderId(command.folderId());

		fileRecord.setUserId(command.userId());

		fileRecord.setMimeType(command.mimeType());

		fileRecord.setName(command.name());

		FileContentRecord fileContentRecord = new FileContentRecord(new FileContentId(fileRecord.id().identifier()));

		fileContentRecord.setFileId(fileRecord.id());

		fileContentRecord.setContent(command.content());

		if (fileStorage.findByFolderId(command.folderId()).stream().anyMatch(file -> file.name().equals(command.name()))) {

			throw new DuplicateFileNameException();
		}

		try {

			fileStorage.insert(fileRecord);

			fileContentStorage.insert(fileContentRecord);

			return fileRecord.id().identifier();

		} catch (DuplicatedDataRecordIdException duplicatedDataRecordIdException) {

			throw new RuntimeException("Unknown exception. Try again.");
		}
	}
}
