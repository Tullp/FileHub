package com.teamdev.services.files;

/**
 * Data transfer object, that needs for represent information about folder.
 */
public class FolderInfo {

	private final String folderId;

	private final String name;

	public FolderInfo(String folderId, String name) {

		this.folderId = folderId;

		this.name = name;
	}

	public String folderId() {

		return folderId;
	}

	public String name() {

		return name;
	}
}
