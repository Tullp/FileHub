package com.teamdev.services.files;

import com.teamdev.persistent.DuplicatedDataRecordIdException;
import com.teamdev.persistent.files.FolderId;
import com.teamdev.persistent.files.FolderRecord;
import com.teamdev.persistent.files.FolderStorage;
import com.teamdev.persistent.user.UserSessionStorage;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.SecuredProcess;

import java.time.Instant;
import java.util.Optional;

/**
 * Implementation of {@link SecuredProcess}. Needs for create folders.<br>
 * Required {@link CreateFolderCommand}.
 */
public class CreateFolderProcess extends SecuredProcess<CreateFolderCommand, String> {

	private final FolderStorage folderStorage;

	public CreateFolderProcess(UserSessionStorage userSessionStorage, FolderStorage folderStorage) {

		super(userSessionStorage);

		this.folderStorage = folderStorage;
	}

	@Override
	public String doHandle(CreateFolderCommand command) throws HandleCommandException {

		Optional<FolderRecord> parentFolder = folderStorage.findById(command.parentFolderId());

		if (!parentFolder.isPresent()) {

			throw new InvalidFolderIdException();
		}

		if (folderStorage.findByParentFolderId(parentFolder.get().id()).stream()
				.anyMatch(folder -> folder.name().equals(command.name()))) {

			throw new DuplicateFolderNameException();
		}

		String path = getFolderPath(parentFolder.get());

		FolderRecord folderRecord = new FolderRecord(new FolderId(String.format(
			"%s:%s:%s:%d", command.userId().identifier(), path, command.name(), Instant.now().getEpochSecond()
		)));

		folderRecord.setName(command.name());
		folderRecord.setParentFolderId(command.parentFolderId());
		folderRecord.setUserId(command.userId());

		try {

			folderStorage.insert(folderRecord);

		} catch (DuplicatedDataRecordIdException exception) {

			throw new RuntimeException("Unknown exception. Try again.");
		}

		return folderRecord.id().identifier();
	}

	private String getFolderPath(FolderRecord folder) {

		StringBuilder path = new StringBuilder(1000);

		FolderRecord currentFolder = folder;

		while (currentFolder.parentFolderId() != null) {

			path.insert(0, currentFolder.name() + '/');

			Optional<FolderRecord> temp = folderStorage.findById(currentFolder.parentFolderId());

			if (!temp.isPresent()) {

				throw new RuntimeException("Unknown exception in getFolderPath: " + folder.id());
			}

			currentFolder = temp.get();
		}

		path.insert(0, '/');

		return path.toString();
	}
}
