package com.teamdev.services;

/**
 * Variety of {@linkplain Command command}, that's using by users with authentication.
 */
public class AuthenticatedUserCommand extends Command {

	protected AuthenticatedUserCommand(AuthToken authToken) { super(authToken); }
}
