package com.teamdev.services;

/**
 * Kind of {@linkplain Process process}, that's calling by users without authentication.
 *
 * @param <C> type of {@linkplain AnonymousUserCommand anonymous command}, that a particular process needs.
 * @param <R> type of value, that process must returns.
 */
public interface OpenProcess<C extends AnonymousUserCommand, R> extends ApplicationProcess<C, R> {

}
