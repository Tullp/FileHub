package com.teamdev.services.user;

import com.teamdev.services.ValueObject;

/**
 * Implementation of {@link ValueObject}, using in {@link RegistrationCommand} and {@link AuthenticationCommand}.
 */
class UserEmail extends ValueObject<String> {

	UserEmail(String content) {

		super(content);
	}
}
