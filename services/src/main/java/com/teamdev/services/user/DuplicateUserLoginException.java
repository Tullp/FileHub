package com.teamdev.services.user;

import com.teamdev.services.HandleCommandException;

/**
 * Throws when anonymous user trying create account with already existent login.
 */
class DuplicateUserLoginException extends HandleCommandException {

	private static final long serialVersionUID = 7435649070944047959L;

	DuplicateUserLoginException() {
		super("User with this login is already exists.");
	}
}
