package com.teamdev.services.user;

import com.google.common.base.Preconditions;

/**
 * Data transfer object, that needs for represent information about authenticated user.
 */
public class UserInfo {

	private final String userId;

	private final String login;

	private final String phone;

	private final String email;

	public UserInfo(String userId, String login, String phone, String email) {

		this.userId = Preconditions.checkNotNull(userId);

		this.login = Preconditions.checkNotNull(login);

		this.phone = Preconditions.checkNotNull(phone);

		this.email = Preconditions.checkNotNull(email);
	}

	public String userId() {

		return userId;
	}

	public String login() {

		return login;
	}

	public String phone() {

		return phone;
	}

	public String email() {

		return email;
	}
}
