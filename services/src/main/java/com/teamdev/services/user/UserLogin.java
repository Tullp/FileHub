package com.teamdev.services.user;

import com.teamdev.services.ValueObject;
import com.teamdev.services.files.CreateFileCommand;

/**
 * Implementation of {@link ValueObject}, using in {@link RegistrationCommand} and {@link AuthenticationCommand}.
 */
class UserLogin extends ValueObject<String> {

	UserLogin(String content) {

		super(content);
	}
}
