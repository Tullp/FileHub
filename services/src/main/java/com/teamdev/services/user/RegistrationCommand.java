package com.teamdev.services.user;

import com.teamdev.services.AnonymousUserCommand;

/**
 * Implementation of {@link AnonymousUserCommand}. Needs for call {@link RegistrationProcess}.
 */
public class RegistrationCommand extends AnonymousUserCommand {

	private final UserLogin login;

	private final UserPassword password;

	private final UserPhone phone;

	private final UserEmail email;

	public RegistrationCommand(String login, String password, String phone, String email) {

		this.login = new UserLogin(login);

		this.password = new UserPassword(password);

		this.phone = new UserPhone(phone);

		this.email = new UserEmail(email);
	}

	public String login() {

		return login.content();
	}

	public String password() {

		return password.content();
	}

	public String phone() {

		return phone.content();
	}

	public String email() {

		return email.content();
	}
}
