package com.teamdev.services.user;

import com.teamdev.services.ValueObject;

/**
 * Implementation of {@link ValueObject}, using in {@link RegistrationCommand} and {@link AuthenticationCommand}.
 */
class UserPassword extends ValueObject<String> {

	UserPassword(String content) {

		super(content);
	}
}
