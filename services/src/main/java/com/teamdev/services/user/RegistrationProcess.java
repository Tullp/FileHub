package com.teamdev.services.user;

import com.google.common.base.Preconditions;
import com.teamdev.persistent.DuplicatedDataRecordIdException;
import com.teamdev.persistent.files.FolderId;
import com.teamdev.persistent.files.FolderRecord;
import com.teamdev.persistent.files.FolderStorage;
import com.teamdev.persistent.user.UserId;
import com.teamdev.persistent.user.UserRecord;
import com.teamdev.persistent.user.UserStorage;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.OpenProcess;

/**
 * Implementation of {@link OpenProcess}. Needs for create new user.<br>
 * Required {@link RegistrationCommand}.
 */
public class RegistrationProcess implements OpenProcess<RegistrationCommand, RegistrationInfo> {

	private final UserStorage userStorage;

	private final FolderStorage folderStorage;

	public RegistrationProcess(UserStorage userStorage, FolderStorage folderStorage) {

		this.userStorage = Preconditions.checkNotNull(userStorage);

		this.folderStorage = folderStorage;
	}

	@Override
	public RegistrationInfo handle(RegistrationCommand command) throws HandleCommandException {

		UserRecord userRecord = new UserRecord(new UserId(command.login()));

		userRecord.setLogin(command.login());
		userRecord.setPassword(command.password());
		userRecord.setPhone(command.phone());
		userRecord.setEmail(command.email());

		FolderRecord folderRecord = new FolderRecord(new FolderId(String.format("%s:/:root", userRecord.id())));

		folderRecord.setName("root");
		folderRecord.setUserId(userRecord.id());
		folderRecord.setParentFolderId(null);

		try {

			userStorage.insert(userRecord);

			folderStorage.insert(folderRecord);

		} catch (DuplicatedDataRecordIdException duplicatedDataRecordIdException) {

			throw new DuplicateUserLoginException();
		}

		return new RegistrationInfo(userRecord.id(), userRecord.login());
	}
}
