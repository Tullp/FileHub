package com.teamdev.services.user;

import com.teamdev.services.ValueObject;

/**
 * Implementation of {@link ValueObject}, using in {@link RegistrationCommand}.
 */
class UserPhone extends ValueObject<String> {

	UserPhone(String content) {

		super(content);
	}
}
