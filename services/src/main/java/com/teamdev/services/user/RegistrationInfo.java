package com.teamdev.services.user;

import com.google.common.base.Preconditions;
import com.teamdev.persistent.user.UserId;

/**
 * Data transfer object, that needs for represent base information about newly created user.
 */
public class RegistrationInfo {

	private final UserId id;

	private final String login;

	public RegistrationInfo(UserId id, String login) {

		this.id = Preconditions.checkNotNull(id);
		this.login = Preconditions.checkNotNull(login);
	}

	public UserId id() {

		return id;
	}

	public String login() {

		return login;
	}
}
