package com.teamdev.services.user;

import com.teamdev.services.HandleCommandException;

/**
 * Throws when anonymous user trying authenticate with non existent pair of login and password.
 */
class InvalidLoginOrPasswordException extends HandleCommandException {

	private static final long serialVersionUID = -7045850203644646690L;

	InvalidLoginOrPasswordException() {

		super("Invalid login or password.");
	}
}
