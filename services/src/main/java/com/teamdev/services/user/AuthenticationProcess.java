package com.teamdev.services.user;

import com.google.common.base.Preconditions;
import com.teamdev.persistent.DuplicatedDataRecordIdException;
import com.teamdev.persistent.user.UserRecord;
import com.teamdev.persistent.user.UserSessionId;
import com.teamdev.persistent.user.UserSessionRecord;
import com.teamdev.persistent.user.UserSessionStorage;
import com.teamdev.persistent.user.UserStorage;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.AuthToken;
import com.teamdev.services.OpenProcess;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

/**
 * Implementation of {@link OpenProcess}. Needs for create session for user.<br>
 * Required {@link AuthenticationCommand}.
 */
public class AuthenticationProcess implements OpenProcess<AuthenticationCommand, AuthToken> {

	private final UserStorage userStorage;

	private final UserSessionStorage userSessionStorage;

	public AuthenticationProcess(UserStorage userStorage, UserSessionStorage userSessionStorage) {

		this.userStorage = Preconditions.checkNotNull(userStorage);

		this.userSessionStorage = Preconditions.checkNotNull(userSessionStorage);
	}

	@Override
	public AuthToken handle(AuthenticationCommand command) throws HandleCommandException {

		Optional<UserRecord> userRecord = userStorage
			.findByLoginAndPassword(command.login(), command.password());

		if (userRecord.isPresent()) {

			String token = String.format("%s:%d", UUID.randomUUID(), Instant.now().getEpochSecond());

			UserSessionRecord userSessionRecord = new UserSessionRecord(new UserSessionId(token));

			userSessionRecord.setUserId(userRecord.get().id());
			userSessionRecord.setToken(token);
			userSessionRecord.setExpiredTime(Instant.now().getEpochSecond() + 60*60*24*30); // + 30 days

			try {

				userSessionStorage.insert(userSessionRecord);

			} catch (DuplicatedDataRecordIdException exception) {

				throw new RuntimeException("Something wrong, try again.");
			}

			return new AuthToken(userSessionRecord.token());
		}

		throw new InvalidLoginOrPasswordException();
	}
}
