package com.teamdev.services.user;

import com.teamdev.services.AnonymousUserCommand;

/**
 * Implementation of {@link AnonymousUserCommand}. Needs for call {@link AuthenticationProcess}.
 */
public class AuthenticationCommand extends AnonymousUserCommand {

	private final UserLogin login;
	private final UserPassword password;

	public AuthenticationCommand(String login, String password) {

		this.login = new UserLogin(login);
		this.password = new UserPassword(password);
	}

	public String login() {

		return login.content();
	}

	public String password() {

		return password.content();
	}
}
