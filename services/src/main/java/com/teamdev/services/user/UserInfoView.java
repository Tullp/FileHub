package com.teamdev.services.user;

import com.google.common.base.Preconditions;
import com.teamdev.persistent.user.UserRecord;
import com.teamdev.persistent.user.UserSessionRecord;
import com.teamdev.persistent.user.UserSessionStorage;
import com.teamdev.persistent.user.UserStorage;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.View;

import java.util.Optional;

/**
 * Implementation of {@link View}. Needs for getting base information about user.<br>
 * Required {@link UserInfoQuery}.
 */
public class UserInfoView extends View<UserInfoQuery, UserInfo> {

	private final UserStorage userStorage;

	public UserInfoView(UserSessionStorage userSessionStorage, UserStorage userStorage) {

		super(userSessionStorage);

		this.userStorage = Preconditions.checkNotNull(userStorage);
	}

	@Override
	protected UserInfo doHandle(UserInfoQuery command) throws HandleCommandException {

		Optional<UserSessionRecord> session = userSessionStorage.findByToken(command.authToken().token());

		if (!session.isPresent()) {

			throw new RuntimeException("Unknown exception.");
		}

		Optional<UserRecord> userRecord = userStorage.findById(session.get().userId());

		if (!userRecord.isPresent()) {

			throw new RuntimeException("Unknown exception");
		}

		return new UserInfo(
			userRecord.get().id().identifier(),
			userRecord.get().login(),
			userRecord.get().phone(),
			userRecord.get().email()
		);
	}
}
