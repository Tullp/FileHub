package com.teamdev.services.user;

import com.teamdev.services.AuthToken;
import com.teamdev.services.Query;
import com.teamdev.services.files.FolderContentView;

/**
 * Implementation of {@link Query}. Needs for call {@link UserInfo}.
 */
public class UserInfoQuery extends Query {

	public UserInfoQuery(AuthToken authToken) {

		super(authToken);
	}
}
