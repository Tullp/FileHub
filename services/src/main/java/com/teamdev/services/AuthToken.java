package com.teamdev.services;

import com.google.common.base.Preconditions;

import java.util.Objects;

/**
 * Representation of {@linkplain com.teamdev.persistent.user.UserSessionRecord session's} token,
 * needs for checking user's authentication.
 */
public class AuthToken {

	private final String token;

	public AuthToken(String token) {

		this.token = Preconditions.checkNotNull(token);
	}

	public String token() {

		return token;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof AuthToken)) return false;
		AuthToken authToken = (AuthToken) o;
		return token.equals(authToken.token);
	}

	@Override
	public int hashCode() {
		return Objects.hash(token);
	}
}
