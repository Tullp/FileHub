package com.teamdev.services;

/**
 * Base abstraction, that represents a process into application. Processes needs for updating data.
 *
 * @param <C> type of {@linkplain Command command}, that a particular process needs.
 * @param <R> type of value, that process must returns.
 */
public interface ApplicationProcess<C extends Command, R> {

	R handle(C command) throws HandleCommandException;
}
