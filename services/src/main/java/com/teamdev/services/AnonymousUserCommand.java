package com.teamdev.services;

/**
 * Variety of {@linkplain Command command}, that's using by users without authentication.
 */
public class AnonymousUserCommand extends Command {

	private static final AuthToken authToken = new AuthToken("Anonymous user.");

	protected AnonymousUserCommand() {
		super(authToken);
	}
}
