package com.teamdev.services;

/**
 * Container with information for getting {@linkplain View views}.
 */
public class Query extends AuthenticatedUserCommand {

	protected Query(AuthToken authToken) {

		super(authToken);
	}
}
