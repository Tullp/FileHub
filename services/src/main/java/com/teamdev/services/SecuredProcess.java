package com.teamdev.services;

import com.teamdev.persistent.user.UserSessionRecord;
import com.teamdev.persistent.user.UserSessionStorage;

import java.time.Instant;
import java.util.Optional;

/**
 * Kind of {@linkplain Process process}, that's calling by users with authentication.
 *
 * @param <C> type of {@linkplain AuthenticatedUserCommand command}, that a particular process needs.
 * @param <R> type of value, that process must returns.
 */
public abstract class SecuredProcess<C extends AuthenticatedUserCommand, R> implements ApplicationProcess<C, R> {

	protected final UserSessionStorage userSessionStorage;

	protected SecuredProcess(UserSessionStorage userSessionStorage) {

		this.userSessionStorage = userSessionStorage;
	}

	private UserSessionStorage userSessionStorage() {

		return userSessionStorage;
	}

	@Override
	public R handle(C command) throws HandleCommandException {

		AuthToken authToken = command.authToken();

		Optional<UserSessionRecord> userSession = userSessionStorage().findByToken(authToken.token());

		if (!userSession.isPresent()) {

			throw new HandleCommandException("Invalid session token.");

		}
		if (userSession.get().expiredTime() - Instant.now().getEpochSecond() <= 0) {

			throw new HandleCommandException("Session expired.");
		}

		return doHandle(command);
	}

	protected abstract R doHandle(C command) throws HandleCommandException;
}
