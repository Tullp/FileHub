package com.teamdev.services;

import com.google.common.base.Preconditions;

/**
 * Container with information for calling {@linkplain Process process}.
 */
class Command {

	private final AuthToken authToken;

	Command(AuthToken authToken) {

		this.authToken = Preconditions.checkNotNull(authToken);
	}

	public AuthToken authToken() {

		return authToken;
	}
}
