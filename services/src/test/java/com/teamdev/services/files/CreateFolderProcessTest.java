package com.teamdev.services.files;

import com.google.common.testing.NullPointerTester;
import com.google.common.truth.Truth;
import com.teamdev.persistent.files.FolderId;
import com.teamdev.persistent.files.FolderRecord;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.testutils.TestEnvironment;
import org.junit.jupiter.api.Test;

import java.util.List;

class CreateFolderProcessTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(CreateFolderProcessTest.class);
	}

	@Test
	void invalidParentFolderIdExceptionTest() throws HandleCommandException {

		FolderRecord folderTemplate = TestEnvironment.createFolderTemplate();

		TestEnvironment testEnvironment = new TestEnvironment();

		try {

			new CreateFolderProcess(testEnvironment.userSessionStorage(), testEnvironment.folderStorage()).doHandle(
				new CreateFolderCommand(
					testEnvironment.authToken(),
					folderTemplate.name(),
					testEnvironment.user().id(),
					new FolderId("Invalid parent folder id.")
				)
			);

		} catch (RuntimeException e) {

			return;
		}

		Truth.assertWithMessage("No exceptions.").fail();
	}

	@Test
	void duplicateFolderNamesExceptionTest() throws HandleCommandException {

		FolderRecord folderTemplate = TestEnvironment.createFolderTemplate();

		TestEnvironment testEnvironment = new TestEnvironment();

		FolderRecord rootFolder = testEnvironment.folderStorage().findRootFolderByUserId(testEnvironment.user().id());

		new CreateFolderProcess(testEnvironment.userSessionStorage(), testEnvironment.folderStorage()).doHandle(
			new CreateFolderCommand(
				testEnvironment.authToken(),
				folderTemplate.name(),
				testEnvironment.user().id(),
				rootFolder.id()
			)
		);

		new CreateFolderProcess(testEnvironment.userSessionStorage(), testEnvironment.folderStorage()).doHandle(
			new CreateFolderCommand(
				testEnvironment.authToken(),
				folderTemplate.name() + '2',
				testEnvironment.user().id(),
				rootFolder.id()
			)
		);

		try {

			new CreateFolderProcess(testEnvironment.userSessionStorage(), testEnvironment.folderStorage()).doHandle(
				new CreateFolderCommand(
					testEnvironment.authToken(),
					folderTemplate.name(),
					testEnvironment.user().id(),
					rootFolder.id()
				)
			);

		} catch (HandleCommandException e) {

			Truth.assertWithMessage("Test duplicate folder name exception.")
				.that(e).isInstanceOf(DuplicateFolderNameException.class);

			return;
		}

		Truth.assertWithMessage("No exceptions.").fail();
	}

	@Test
	void positiveUnitTest() throws HandleCommandException {

		FolderRecord folderTemplate = TestEnvironment.createFolderTemplate();

		TestEnvironment testEnvironment = new TestEnvironment();

		FolderRecord rootFolder = testEnvironment.folderStorage().findRootFolderByUserId(testEnvironment.user().id());

		new CreateFolderProcess(testEnvironment.userSessionStorage(), testEnvironment.folderStorage()).doHandle(
			new CreateFolderCommand(
				testEnvironment.authToken(),
				folderTemplate.name(),
				testEnvironment.user().id(),
				rootFolder.id()
			)
		);

		List<FolderRecord> foldersInRoot = testEnvironment.folderStorage().findByParentFolderId(rootFolder.id());

		Truth.assertWithMessage("Check folders in root count.")
			.that(foldersInRoot.size()).isEqualTo(1);

		FolderRecord newFolder = foldersInRoot.get(0);

		Truth.assertWithMessage("Test new folder's parent id.")
			.that(newFolder.parentFolderId()).isEqualTo(rootFolder.id());

		Truth.assertWithMessage("Test new folder's name.")
			.that(newFolder.name()).isEqualTo(folderTemplate.name());

		Truth.assertWithMessage("Test new folder's owner id.")
			.that(newFolder.ownerId()).isEqualTo(testEnvironment.user().id());

		Truth.assertWithMessage("Test new folder's id.").that(newFolder.id().identifier().matches(
			String.format(
				"%s:/:%s:\\d+",
				testEnvironment.user().id().identifier(),
				folderTemplate.name()
			)
		)).isTrue();
	}
}