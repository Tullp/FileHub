package com.teamdev.services.files;

import com.google.common.testing.NullPointerTester;
import com.google.common.truth.Truth;
import com.teamdev.persistent.files.FileContentRecord;
import com.teamdev.persistent.files.FileId;
import com.teamdev.persistent.files.FileRecord;
import com.teamdev.persistent.files.FolderId;
import com.teamdev.persistent.files.FolderRecord;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.testutils.TestEnvironment;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class CreateFileProcessTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(CreateFileProcessTest.class);
	}

	@Test
	void negativeTest() throws HandleCommandException {

		FileRecord fileTemplate = TestEnvironment.createFileTemplate();

		FileContentRecord fileContentRecord = TestEnvironment.createFileContentTemplate();

		TestEnvironment testEnvironment = new TestEnvironment();

		FolderId folderId = testEnvironment.createFolderInRoot(TestEnvironment.createFolderTemplate().name());

		new CreateFileProcess(

			testEnvironment.userSessionStorage(),
			testEnvironment.fileStorage(),
			testEnvironment.fileContentStorage(),
			testEnvironment.folderStorage()

		).handle(new CreateFileCommandBuilder()

			.setAuthToken(testEnvironment.authToken())
			.setName(fileTemplate.name())
			.setMimeType(fileTemplate.mimeType())
			.setFolderId(folderId)
			.setUserId(testEnvironment.user().id())
			.setContent(fileContentRecord.content())
			.build()
		);

		try {

			new CreateFileProcess(

				testEnvironment.userSessionStorage(),
				testEnvironment.fileStorage(),
				testEnvironment.fileContentStorage(),
				testEnvironment.folderStorage()

			).handle(new CreateFileCommandBuilder()

				.setAuthToken(testEnvironment.authToken())
				.setName(fileTemplate.name())
				.setMimeType(fileTemplate.mimeType())
				.setFolderId(folderId)
				.setUserId(testEnvironment.user().id())
				.setContent(fileContentRecord.content())
				.build()
			);

		} catch (HandleCommandException e) {

			Truth.assertWithMessage("Test duplicate file name exception.")
				.that(e).isInstanceOf(DuplicateFileNameException.class);

			return;
		}

		Truth.assertWithMessage("No exceptions.").fail();
	}

	@Test
	void positiveUnitTest() throws HandleCommandException {

		FileRecord fileTemplate = TestEnvironment.createFileTemplate();

		FileContentRecord fileContentRecord = TestEnvironment.createFileContentTemplate();

		FolderRecord folderTemplate = TestEnvironment.createFolderTemplate();

		TestEnvironment testEnvironment = new TestEnvironment();

		FolderId folderId = testEnvironment.createFolderInRoot(folderTemplate.name());

		FileId fileId = new FileId(new CreateFileProcess(

			testEnvironment.userSessionStorage(),
			testEnvironment.fileStorage(),
			testEnvironment.fileContentStorage(),
			testEnvironment.folderStorage()

		).handle(new CreateFileCommand(

			testEnvironment.authToken(),
			fileTemplate.name(),
			fileTemplate.mimeType(),
			folderId,
			testEnvironment.user().id(),
			fileContentRecord.content()
		)));

		Optional<FileRecord> tempFile = testEnvironment.fileStorage().findById(fileId);

		Truth.assertWithMessage("Test is file exists.")
			.that(tempFile.isPresent()).isTrue();

		FileRecord file = tempFile.get();

		Optional<FileContentRecord> tempContent = testEnvironment.fileContentStorage().findByFileId(fileId);

		Truth.assertWithMessage("Test is file content exists.")
			.that(tempContent.isPresent()).isTrue();

		FileContentRecord fileContent = tempContent.get();

		Truth.assertWithMessage("Test file id.")
			.that(file.id().identifier().matches(String.format(
				"%s:%s:\\d+",
				folderId.identifier(),
				fileTemplate.name()
			))).isTrue();

		Truth.assertWithMessage("Test file content.")
			.that(fileContent.content()).isEqualTo(fileContent.content());

		Truth.assertWithMessage("Test file folder id.")
			.that(file.folderId()).isEqualTo(folderId);

		Truth.assertWithMessage("Test file name.")
			.that(file.name()).isEqualTo(fileTemplate.name());

		Truth.assertWithMessage("Test file owner id.")
			.that(file.userId()).isEqualTo(testEnvironment.user().id());

		Truth.assertWithMessage("Test file mime type.")
			.that(file.mimeType()).isEqualTo(fileTemplate.mimeType());
	}
}