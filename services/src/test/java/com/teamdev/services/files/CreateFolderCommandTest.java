package com.teamdev.services.files;

import com.google.common.testing.NullPointerTester;
import org.junit.jupiter.api.Test;

class CreateFolderCommandTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(CreateFolderCommandTest.class);
	}
}