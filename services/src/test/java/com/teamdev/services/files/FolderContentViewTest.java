package com.teamdev.services.files;

import com.google.common.testing.NullPointerTester;
import com.google.common.truth.Truth;
import com.teamdev.persistent.files.FolderId;
import com.teamdev.persistent.files.FolderRecord;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.testutils.TestEnvironment;
import org.junit.jupiter.api.Test;

class FolderContentViewTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(FolderContentViewTest.class);
	}

	@Test
	void positiveUnitTest() throws HandleCommandException {

		FolderRecord folderTemplate = TestEnvironment.createFolderTemplate();

		TestEnvironment testEnvironment = new TestEnvironment();

		FolderId folderId = testEnvironment.createFolderInRoot(folderTemplate.name());

		for (int i = 0; i < 5; i++) {

			testEnvironment.createFile(i + ".txt", folderId);
		}

		FolderContent folderContent = new FolderContentView(

			testEnvironment.userSessionStorage(),
			testEnvironment.fileStorage(),
			testEnvironment.folderStorage()

		).doHandle(new FolderContentQuery(testEnvironment.authToken(), folderId.identifier()));

		for (int i = 0; i < 5; i++) {

			Truth.assertWithMessage("Test " + i + " file name.")
				.that(folderContent.files().get(i).name()).isEqualTo(i + ".txt");
		}
	}
}