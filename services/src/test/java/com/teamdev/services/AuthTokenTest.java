package com.teamdev.services;

import com.google.common.testing.NullPointerTester;
import org.junit.jupiter.api.Test;

class AuthTokenTest {

	@Test
	void testNotNull() {

		new NullPointerTester().testAllPublicConstructors(AuthToken.class);
	}
}