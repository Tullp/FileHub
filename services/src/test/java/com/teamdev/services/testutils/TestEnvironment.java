package com.teamdev.services.testutils;

import com.teamdev.persistent.files.FileContentId;
import com.teamdev.persistent.files.FileContentInMemoryStorage;
import com.teamdev.persistent.files.FileContentRecord;
import com.teamdev.persistent.files.FileContentStorage;
import com.teamdev.persistent.files.FileId;
import com.teamdev.persistent.files.FileInMemoryStorage;
import com.teamdev.persistent.files.FileRecord;
import com.teamdev.persistent.files.FileStorage;
import com.teamdev.persistent.files.FolderId;
import com.teamdev.persistent.files.FolderInMemoryStorage;
import com.teamdev.persistent.files.FolderRecord;
import com.teamdev.persistent.files.FolderStorage;
import com.teamdev.persistent.user.UserId;
import com.teamdev.persistent.user.UserInMemoryStorage;
import com.teamdev.persistent.user.UserRecord;
import com.teamdev.persistent.user.UserSessionInMemoryStorage;
import com.teamdev.persistent.user.UserSessionStorage;
import com.teamdev.persistent.user.UserStorage;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.AuthToken;
import com.teamdev.services.files.CreateFileCommand;
import com.teamdev.services.files.CreateFileProcess;
import com.teamdev.services.files.CreateFolderCommand;
import com.teamdev.services.files.CreateFolderProcess;
import com.teamdev.services.user.AuthenticationCommand;
import com.teamdev.services.user.AuthenticationProcess;
import com.teamdev.services.user.RegistrationCommand;
import com.teamdev.services.user.RegistrationInfo;
import com.teamdev.services.user.RegistrationProcess;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

public class TestEnvironment {

	private final UserStorage userStorage = new UserInMemoryStorage();

	private final UserSessionStorage userSessionStorage = new UserSessionInMemoryStorage();

	private final FolderStorage folderStorage = new FolderInMemoryStorage();

	private final FileStorage fileStorage = new FileInMemoryStorage();

	private final FileContentStorage fileContentStorage = new FileContentInMemoryStorage();

	private final UserRecord user;

	private final AuthToken authToken;

	public TestEnvironment() throws HandleCommandException {

		user = registerUser();

		authToken = authenticateUser();
	}

	public FolderId createFolderInRoot(String folderName) throws HandleCommandException {

		return new FolderId(new CreateFolderProcess(userSessionStorage, folderStorage).doHandle(new CreateFolderCommand(
			authToken, folderName, user.id(), folderStorage().findRootFolderByUserId(user.id()).id()
		)));
	}

	public FileId createFile(String fileName, FolderId folderId) throws HandleCommandException {

		return new FileId(new CreateFileProcess(userSessionStorage, fileStorage, fileContentStorage, folderStorage).handle(
			new CreateFileCommand(
				authToken,
				fileName,
				fileName,
				folderId,
				user.id(),
				"Empty.".getBytes(StandardCharsets.UTF_8)
			)
		));
	}

	private UserRecord registerUser() throws HandleCommandException {

		UserRecord userRecord = createUserTemplate();

		RegistrationInfo info = new RegistrationProcess(userStorage, folderStorage).handle(new RegistrationCommand(
			userRecord.login(), userRecord.password(), userRecord.phone(), userRecord.email()
		));

		Optional<UserRecord> temp = userStorage.findById(info.id());

		if (!temp.isPresent()) {

			throw new RuntimeException("Can't find user by id.");
		}

		return temp.get();
	}

	private AuthToken authenticateUser() throws HandleCommandException {

		return new AuthenticationProcess(userStorage, userSessionStorage).handle(
			new AuthenticationCommand(user.login(), user.password())
		);
	}

	public UserStorage userStorage() {

		return userStorage;
	}

	public UserSessionStorage userSessionStorage() {

		return userSessionStorage;
	}

	public FileStorage fileStorage() {

		return fileStorage;
	}

	public FolderStorage folderStorage() {

		return folderStorage;
	}

	public FileContentStorage fileContentStorage() {

		return fileContentStorage;
	}

	public UserRecord user() {

		return user;
	}

	public AuthToken authToken() {

		return authToken;
	}

	public static UserRecord createUserTemplate() {

		UserRecord userRecord = new UserRecord(new UserId("Test login."));
		userRecord.setLogin(userRecord.id().identifier());
		userRecord.setEmail("Test email.");
		userRecord.setPassword("Test password.");
		userRecord.setPhone("Test phone.");

		return userRecord;
	}

	public static FileRecord createFileTemplate() {

		FileRecord fileRecord = new FileRecord(new FileId(""));

		fileRecord.setName("test.txt");

		fileRecord.setMimeType("txt");

		return fileRecord;
	}

	public static FileContentRecord createFileContentTemplate() {

		FileContentRecord fileContentRecord = new FileContentRecord(new FileContentId(""));

		fileContentRecord.setContent("Hello World!".getBytes(StandardCharsets.UTF_8));

		return fileContentRecord;
	}

	public static FolderRecord createFolderTemplate() {

		FolderRecord folderRecord = new FolderRecord(new FolderId(""));

		folderRecord.setName("Test folder.");

		return folderRecord;
	}
}
