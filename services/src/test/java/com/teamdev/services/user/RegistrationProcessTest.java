package com.teamdev.services.user;

import com.google.common.testing.NullPointerTester;
import com.google.common.truth.Truth;
import com.teamdev.persistent.files.FolderInMemoryStorage;
import com.teamdev.persistent.files.FolderRecord;
import com.teamdev.persistent.user.UserInMemoryStorage;
import com.teamdev.persistent.user.UserRecord;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.testutils.TestEnvironment;
import org.junit.jupiter.api.Test;

class RegistrationProcessTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(RegistrationProcessTest.class);
	}

	@Test
	void negativeUnitTest() throws HandleCommandException {

		TestEnvironment testEnvironment = new TestEnvironment();

		UserRecord user = testEnvironment.user();

		try {

			new RegistrationProcess(testEnvironment.userStorage(), testEnvironment.folderStorage()).handle(
				new RegistrationCommand(user.login(), user.password(), user.phone(), user.email())
			);

		} catch (HandleCommandException exception) {

			Truth.assertWithMessage("Test duplicate user login exception.")
				.that(exception).isInstanceOf(DuplicateUserLoginException.class);

			return;
		}

		Truth.assertWithMessage("No exception.").fail();
	}

	@Test
	void positiveUnitTest() throws HandleCommandException {

		UserRecord testUserRecord = TestEnvironment.createUserTemplate();

		RegistrationCommand command = new RegistrationCommand(
			testUserRecord.login(),
			testUserRecord.password(),
			testUserRecord.phone(),
			testUserRecord.email()
		);

		final boolean[] isInsertInUserStorageCalled = {false};

		final boolean[] isInsertInFolderStorageCalled = {true};

		new RegistrationProcess(

			new UserInMemoryStorage() {

				@Override
				public void insert(UserRecord userRecord) {

					isInsertInUserStorageCalled[0] = true;

					Truth.assertWithMessage("Test user id's equality.")
						.that(userRecord.id()).isEqualTo(testUserRecord.id());

					Truth.assertWithMessage("Test user logins equality.")
						.that(userRecord.login()).isEqualTo(testUserRecord.login());

					Truth.assertWithMessage("Test user passwords equality.")
						.that(userRecord.password()).isEqualTo(testUserRecord.password());

					Truth.assertWithMessage("Test user phones equality.")
						.that(userRecord.phone()).isEqualTo(testUserRecord.phone());

					Truth.assertWithMessage("Test user emails equality.")
						.that(userRecord.email()).isEqualTo(testUserRecord.email());
				}
			},

			new FolderInMemoryStorage() {

				@Override
				public void insert(FolderRecord folderRecord) {

					isInsertInFolderStorageCalled[0] = true;


					Truth.assertWithMessage("Test folder name correctly.")
						.that(folderRecord.name()).isEqualTo("root");

					Truth.assertWithMessage("Test folder parent id correctly.")
						.that(folderRecord.parentFolderId()).isEqualTo(null);

					Truth.assertWithMessage("Test folder owner id correctly.")
						.that(folderRecord.ownerId()).isEqualTo(testUserRecord.id());

					Truth.assertWithMessage("Test folder id correctly.")
						.that(folderRecord.id().identifier()).isEqualTo(testUserRecord.id() + ":/:" + "root");
				}
			}).handle(command);

		Truth.assertWithMessage("Test is users storage insert was called.")
			.that(isInsertInUserStorageCalled[0]).isEqualTo(true);

		Truth.assertWithMessage("Test is folders storage insert was called.")
			.that(isInsertInFolderStorageCalled[0]).isEqualTo(true);
	}
}