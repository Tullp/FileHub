package com.teamdev.services.user;

import com.google.common.testing.NullPointerTester;
import org.junit.jupiter.api.Test;

class UserPhoneTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(UserPhoneTest.class);
	}
}