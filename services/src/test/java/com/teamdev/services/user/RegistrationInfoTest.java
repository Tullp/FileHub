package com.teamdev.services.user;

import com.google.common.testing.NullPointerTester;
import org.junit.jupiter.api.Test;

class RegistrationInfoTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(RegistrationInfoTest.class);
	}
}