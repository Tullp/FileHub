package com.teamdev.services.user;

import com.google.common.testing.NullPointerTester;
import com.google.common.truth.Truth;
import com.teamdev.persistent.DuplicatedDataRecordIdException;
import com.teamdev.persistent.user.UserInMemoryStorage;
import com.teamdev.persistent.user.UserRecord;
import com.teamdev.persistent.user.UserSessionInMemoryStorage;
import com.teamdev.persistent.user.UserSessionStorage;
import com.teamdev.persistent.user.UserStorage;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.AuthToken;
import com.teamdev.services.testutils.TestEnvironment;
import org.junit.jupiter.api.Test;

class AuthenticationProcessTest {

	@Test
	void testAllPublicConstructors() {

		new NullPointerTester().testAllPublicConstructors(AuthenticationProcessTest.class);
	}

	@Test
	void negativeUnitTest() {

		try {

			new AuthenticationProcess(
				new UserInMemoryStorage(), new UserSessionInMemoryStorage()
			).handle(new AuthenticationCommand("Non existen login.", "Non existen password."));

		} catch (HandleCommandException e) {

			Truth.assertWithMessage("Test invalid login or password exception.")
				.that(e).isInstanceOf(InvalidLoginOrPasswordException.class);

			return;
		}

		Truth.assertWithMessage("There are no exceptions.").fail();
	}

	@Test
	void positiveUnitTest() throws HandleCommandException, DuplicatedDataRecordIdException {

		UserStorage userStorage = new UserInMemoryStorage();

		UserSessionStorage userSessionStorage = new UserSessionInMemoryStorage();

		UserRecord userRecord = TestEnvironment.createUserTemplate();

		userStorage.insert(userRecord);

		AuthenticationCommand command = new AuthenticationCommand(userRecord.login(), userRecord.password());

		AuthToken authToken = new AuthenticationProcess(userStorage, userSessionStorage).handle(command);

		Truth.assertWithMessage("Test is token contains in storage.")
			.that(userSessionStorage.findByToken(authToken.token()).isPresent())
			.isTrue();

		Truth.assertWithMessage("Test is token userId correctly.")
			.that(userSessionStorage.findByToken(authToken.token()).get().userId())
			.isEqualTo(userRecord.id());
	}
}