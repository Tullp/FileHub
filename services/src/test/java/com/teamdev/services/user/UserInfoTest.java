package com.teamdev.services.user;

import com.google.common.testing.NullPointerTester;
import org.junit.jupiter.api.Test;

class UserInfoTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(UserInfoTest.class);
	}

}