package com.teamdev.services.user;

import com.google.common.testing.NullPointerTester;
import com.google.common.truth.Truth;
import com.teamdev.services.HandleCommandException;
import com.teamdev.services.testutils.TestEnvironment;
import org.junit.jupiter.api.Test;

class UserInfoViewTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(UserInfoViewTest.class);
	}

	@Test
	void positiveUnitTest() throws HandleCommandException {

		TestEnvironment testEnvironment = new TestEnvironment();

		UserInfo userInfo = new UserInfoView(testEnvironment.userSessionStorage(), testEnvironment.userStorage()).doHandle(
			new UserInfoQuery(testEnvironment.authToken())
		);

		Truth.assertWithMessage("Test user's id.")
			.that(testEnvironment.user().id().identifier()).isEqualTo(userInfo.userId());

		Truth.assertWithMessage("Test user's login.")
			.that(testEnvironment.user().login()).isEqualTo(userInfo.login());

		Truth.assertWithMessage("Test user's phone.")
			.that(testEnvironment.user().phone()).isEqualTo(userInfo.phone());

		Truth.assertWithMessage("Test user's email.")
			.that(testEnvironment.user().email()).isEqualTo(userInfo.email());
	}

}