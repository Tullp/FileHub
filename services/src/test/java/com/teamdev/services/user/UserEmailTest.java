package com.teamdev.services.user;

import com.google.common.testing.NullPointerTester;
import org.junit.jupiter.api.Test;

class UserEmailTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(UserEmailTest.class);
	}
}