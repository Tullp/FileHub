package com.teamdev.persistent.user;

import com.google.common.testing.NullPointerTester;
import org.junit.jupiter.api.Test;

class UserSessionInMemoryStorageTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(UserSessionInMemoryStorageTest.class);

	    new NullPointerTester().testAllPublicInstanceMethods(new UserSessionInMemoryStorage());
	}
}