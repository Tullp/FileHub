package com.teamdev.persistent.user;

import com.google.common.testing.NullPointerTester;
import org.junit.jupiter.api.Test;

class UserInMemoryStorageTest {

	@Test
	void testAllPublicConstructors() {

		new NullPointerTester().testAllPublicConstructors(UserInMemoryStorageTest.class);

		new NullPointerTester().testAllPublicInstanceMethods(new UserInMemoryStorage());
	}
}