package com.teamdev.persistent.user;

import com.google.common.testing.NullPointerTester;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class UserSessionRecordTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(UserSessionRecordTest.class);

	    new NullPointerTester().testAllPublicInstanceMethods(
	    	new UserSessionRecord(new UserSessionId(UUID.randomUUID().toString()))
	    );
	}
}