package com.teamdev.persistent.user;

import com.google.common.testing.NullPointerTester;
import org.junit.jupiter.api.Test;

class UserSessionIdTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(UserSessionIdTest.class);
	}
}