package com.teamdev.persistent.user;

import com.google.common.testing.NullPointerTester;
import org.junit.jupiter.api.Test;

class UserRecordTest {

	@Test
	void testAllPublicConstructors() {

	    new NullPointerTester().testAllPublicConstructors(UserRecordTest.class);

	    new NullPointerTester().testAllPublicInstanceMethods(
	    	new UserRecord(new UserId(""))
	    );
	}
}