package com.teamdev.persistent;

import com.google.common.base.Preconditions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * {@linkplain Storage Storage} implementation, that implements CRUD operations in memory environment.
 *
 * @param <D> type of stored {@linkplain DataRecord records}.
 * @param <I> type of {@linkplain DataRecordId identifier} for stored {@linkplain DataRecord records}.
 */
public class AbstractInMemoryStorage<D extends DataRecord<I>, I extends DataRecordId<?>> implements Storage<D, I> {

	private final List<D> records = new ArrayList<>();

	protected List<D> records() {

		return Collections.unmodifiableList(records);
	}

	@Override
	public void insert(D dataRecord) throws DuplicatedDataRecordIdException {

		Preconditions.checkNotNull(dataRecord);

		if (!findById(dataRecord.id()).isPresent()) {

			records.add(dataRecord);

		} else {

			throw new DuplicatedDataRecordIdException();
		}
	}

	@Override
	public Optional<D> findById(I dataRecordId) {

		Preconditions.checkNotNull(dataRecordId);

		return records.stream().filter(record -> record.id().equals(dataRecordId)).findFirst();
	}

	@Override
	public void update(D dataRecord) {

		Preconditions.checkNotNull(dataRecord);

		Optional<D> currentRecord = findById(dataRecord.id());

		currentRecord.ifPresent(d -> records.set(records.indexOf(d), dataRecord));

	}

	@Override
	public boolean delete(I dataRecordId) {

		Preconditions.checkNotNull(dataRecordId);

		Optional<D> record = findById(dataRecordId);

		if (record.isPresent()) {

			records.remove(record.get());

			return true;
		}

		return false;
	}
}
