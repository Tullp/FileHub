package com.teamdev.persistent.files;

import com.teamdev.persistent.DataRecord;
import com.teamdev.persistent.user.UserId;

/**
 * Implementation of {@link DataRecord}, that contains information about files.
 */
public class FileRecord extends DataRecord<FileId> {

	private String name;

	private String mimeType;

	private FolderId folderId;

	private UserId userId;

	public String name() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String mimeType() {

		return mimeType;
	}

	public void setMimeType(String mimeType) {

		this.mimeType = mimeType;
	}

	public FolderId folderId() {

		return folderId;
	}

	public void setFolderId(FolderId folderId) {

		this.folderId = folderId;
	}

	public UserId userId() {

		return userId;
	}

	public void setUserId(UserId userId) {

		this.userId = userId;
	}

	public FileRecord(FileId id) {

		super(id);
	}
}
