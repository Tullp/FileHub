package com.teamdev.persistent.files;

import com.teamdev.persistent.DataRecordId;

/**
 * Implementation of {@link DataRecordId}, that identifies {@linkplain FileContentRecord files contents records}.<br>
 *
 * Contains id of file to which record are mentioned.
 */
public class FileContentId extends DataRecordId<String> {

	public FileContentId(String identifier) {

		super(identifier);
	}
}
