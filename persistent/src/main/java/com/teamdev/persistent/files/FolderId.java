package com.teamdev.persistent.files;

import com.teamdev.persistent.DataRecordId;
import com.teamdev.persistent.user.UserSessionRecord;

/**
 * Implementation of {@link DataRecordId}, that identifies {@linkplain FolderRecord folders records}.<br>
 *
 * Contains id of user, to with folder is mentioned, path to this folder, folder's name and timestamp of creation.
 */
public class FolderId extends DataRecordId<String> {

	public FolderId(String identifier) {

		super(identifier);
	}
}
