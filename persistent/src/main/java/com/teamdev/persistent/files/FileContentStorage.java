package com.teamdev.persistent.files;

import com.teamdev.persistent.Storage;
import com.teamdev.persistent.user.UserRecord;

import java.util.Optional;

/**
 * Addition to {@linkplain Storage base storage interface},
 * that contains methods for interactions with {@linkplain FileContentRecord files contents records}.
 */
public interface FileContentStorage extends Storage<FileContentRecord, FileContentId> {

	Optional<FileContentRecord> findByFileId(FileId fileId);
}
