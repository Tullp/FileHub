package com.teamdev.persistent.files;

import com.teamdev.persistent.DataRecordId;

/**
 * Implementation of {@link DataRecordId}, that identifies {@linkplain FileRecord files records}.<br>
 *
 * Contains folder id, to wich file is mentioned, name of file and timestamp of creation.
 */
public class FileId extends DataRecordId<String> {

	public FileId(String identifier) {

		super(identifier);
	}
}
