package com.teamdev.persistent.files;

import com.teamdev.persistent.Storage;
import com.teamdev.persistent.user.UserRecord;

import java.util.List;

/**
 * Addition to {@linkplain Storage base storage interface},
 * that contains methods for interactions with {@linkplain FileRecord files records}.
 */
public interface FileStorage extends Storage<FileRecord, FileId> {

	List<FileRecord> findByFolderId(FolderId folderId);
}
