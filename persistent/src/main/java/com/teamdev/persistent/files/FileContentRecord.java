package com.teamdev.persistent.files;

import com.teamdev.persistent.DataRecord;

/**
 * Implementation of {@link DataRecord}, that contains file's content.
 */
public class FileContentRecord extends DataRecord<FileContentId> {

	private FileId fileId;

	private byte[] content;

	public FileId fileId() {

		return fileId;
	}

	public void setFileId(FileId fileId) {

		this.fileId = fileId;
	}

	public byte[] content() {

		return content.clone();
	}

	public void setContent(byte[] content) {

		this.content = content.clone();
	}

	public FileContentRecord(FileContentId id) {

		super(id);
	}
}
