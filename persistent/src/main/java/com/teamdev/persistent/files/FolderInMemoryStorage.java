package com.teamdev.persistent.files;

import com.teamdev.persistent.AbstractInMemoryStorage;
import com.teamdev.persistent.user.UserId;
import com.teamdev.persistent.user.UserSessionRecord;
import com.teamdev.persistent.user.UserSessionStorage;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of {@link FolderStorage}, that based on {@link AbstractInMemoryStorage}.<br>
 * Represents of memory storage for {@linkplain FolderRecord folders records}.
 */
public class FolderInMemoryStorage extends AbstractInMemoryStorage<FolderRecord, FolderId> implements FolderStorage {

	@Override
	public List<FolderRecord> findByParentFolderId(FolderId folderId) {

		return records().stream().filter(
			folder -> folder.parentFolderId() != null ?
				folder.parentFolderId().equals(folderId) : folderId == null
		).collect(Collectors.toList());
	}

	@Override
	public FolderRecord findRootFolderByUserId(UserId userId) {

		Optional<FolderRecord> rootFolder = records().stream()
			.filter(folder -> folder.ownerId().equals(userId) && folder.parentFolderId() == null)
			.findFirst();

		if (rootFolder.isPresent()) {

			return rootFolder.get();
		}

		throw new RuntimeException("Can't found root folder for " + userId + '.');
	}
}
