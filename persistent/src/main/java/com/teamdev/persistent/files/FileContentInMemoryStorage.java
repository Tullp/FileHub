package com.teamdev.persistent.files;

import com.teamdev.persistent.AbstractInMemoryStorage;
import com.teamdev.persistent.user.UserSessionRecord;
import com.teamdev.persistent.user.UserSessionStorage;

import java.util.Optional;

/**
 * Implementation of {@link FileContentStorage}, that based on {@link AbstractInMemoryStorage}.<br>
 * Represents of memory storage for {@linkplain FileContentRecord files contents records}.
 */
public class FileContentInMemoryStorage
		extends AbstractInMemoryStorage<FileContentRecord, FileContentId>
		implements FileContentStorage {

	@Override
	public Optional<FileContentRecord> findByFileId(FileId fileId) {

		return records().stream()
			.filter(fileContent -> fileContent.fileId().equals(fileId))
			.findFirst();
	}
}
