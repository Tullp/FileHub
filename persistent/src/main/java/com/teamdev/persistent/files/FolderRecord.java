package com.teamdev.persistent.files;

import com.teamdev.persistent.DataRecord;
import com.teamdev.persistent.user.UserId;

/**
 * Implementation of {@link DataRecord}, that contains information about folders.
 */
public class FolderRecord extends DataRecord<FolderId> {

	private String name;

	private FolderId parentFolderId;

	private UserId userId;

	public FolderRecord(FolderId id) {

		super(id);
	}

	public String name() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public FolderId parentFolderId() {

		return parentFolderId;
	}

	public void setParentFolderId(FolderId parentFolderId) {

		this.parentFolderId = parentFolderId;
	}

	public UserId ownerId() {

		return userId;
	}

	public void setUserId(UserId userId) {

		this.userId = userId;
	}
}
