package com.teamdev.persistent.files;

import com.teamdev.persistent.AbstractInMemoryStorage;
import com.teamdev.persistent.user.UserSessionRecord;
import com.teamdev.persistent.user.UserSessionStorage;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of {@link FileStorage}, that based on {@link AbstractInMemoryStorage}.<br>
 * Represents of memory storage for {@linkplain FileRecord files records}.
 */
public class FileInMemoryStorage extends AbstractInMemoryStorage<FileRecord, FileId> implements FileStorage {

	@Override
	public List<FileRecord> findByFolderId(FolderId folderId) {

		return records().stream().filter(file -> file.folderId().equals(folderId)).collect(Collectors.toList());
	}
}
