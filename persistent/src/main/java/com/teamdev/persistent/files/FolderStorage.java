package com.teamdev.persistent.files;

import com.teamdev.persistent.Storage;
import com.teamdev.persistent.user.UserId;
import com.teamdev.persistent.user.UserRecord;

import java.util.List;

/**
 * Addition to {@linkplain Storage base storage interface},
 * that contains methods for interactions with {@linkplain FolderRecord folders records}.
 */
public interface FolderStorage extends Storage<FolderRecord, FolderId> {

	List<FolderRecord> findByParentFolderId(FolderId folderId);

	FolderRecord findRootFolderByUserId(UserId userId);
}
