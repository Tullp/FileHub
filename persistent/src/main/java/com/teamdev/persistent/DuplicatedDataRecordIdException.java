package com.teamdev.persistent;

/**
 * Throws during attempt of insert {@linkplain DataRecord data record}
 * with already existent {@linkplain DataRecordId id} in {@linkplain Storage storage} implementation.
 */
public class DuplicatedDataRecordIdException extends Exception {

	private static final long serialVersionUID = -7247520114700602285L;
}
