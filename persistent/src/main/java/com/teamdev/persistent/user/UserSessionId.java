package com.teamdev.persistent.user;

import com.teamdev.persistent.DataRecordId;

/**
 * Implementation of {@link DataRecordId}, that identifies {@linkplain UserSessionRecord sessions records}.<br>
 *
 * Contains UUID and timestamp of creation.
 */
public class UserSessionId extends DataRecordId<String> {

	public UserSessionId(String token) {

		super(token);
	}
}
