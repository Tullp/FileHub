package com.teamdev.persistent.user;

import com.google.common.base.Preconditions;
import com.teamdev.persistent.DataRecord;

/**
 * Implementation of {@link DataRecord}, that contains base users parameters.
 */
public class UserRecord extends DataRecord<UserId> {

	private String login;

	private String password;

	private String phone;

	private String email;

	public UserRecord(UserId id) {

		super(id);
	}

	public String login() {

		return login;
	}

	public void setLogin(String login) {

		this.login = Preconditions.checkNotNull(login);
	}

	public String password() {

		return password;
	}

	public void setPassword(String password) {

		this.password = Preconditions.checkNotNull(password);
	}

	public String phone() {

		return phone;
	}

	public void setPhone(String phone) {

		this.phone = Preconditions.checkNotNull(phone);
	}

	public String email() {

		return email;
	}

	public void setEmail(String email) {

		this.email = Preconditions.checkNotNull(email);
	}
}
