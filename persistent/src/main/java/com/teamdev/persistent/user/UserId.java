package com.teamdev.persistent.user;

import com.teamdev.persistent.DataRecordId;

/**
 * Implementation of {@link DataRecordId}, that identifies {@linkplain UserRecord users records}.<br>
 *
 * Contains user login.
 */
public class UserId extends DataRecordId<String> {

	public UserId(String identifier) {

		super(identifier);
	}
}
