package com.teamdev.persistent.user;

import com.google.common.base.Preconditions;
import com.teamdev.persistent.AbstractInMemoryStorage;

import java.util.Optional;

/**
 * Implementation of {@link UserSessionStorage}, that based on {@link AbstractInMemoryStorage}.<br>
 * Represents of memory storage for {@linkplain UserSessionRecord sessions records}.
 */
public class UserSessionInMemoryStorage
		extends AbstractInMemoryStorage<UserSessionRecord, UserSessionId>
		implements UserSessionStorage {

	@Override
	public Optional<UserSessionRecord> findByToken(String token) {

		Preconditions.checkNotNull(token);

		return records().stream()
			.filter(session -> session.token().equals(token))
			.findFirst();
	}
}
