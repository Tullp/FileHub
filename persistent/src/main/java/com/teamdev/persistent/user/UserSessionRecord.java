package com.teamdev.persistent.user;

import com.google.common.base.Preconditions;
import com.teamdev.persistent.DataRecord;

/**
 * Implementation of {@link DataRecord}, that contains information about user's sessions, which authenticate on client side.
 */
public class UserSessionRecord extends DataRecord<UserSessionId> {

	private String token;

	private UserId userId;

	private long expiredTime;

	public UserSessionRecord(UserSessionId id) {

		super(id);
	}

	public String token() {

		return token;
	}

	public void setToken(String token) {

		this.token = Preconditions.checkNotNull(token);
	}

	public UserId userId() {

		return userId;
	}

	public void setUserId(UserId userId) {

		this.userId = Preconditions.checkNotNull(userId);
	}

	public long expiredTime() {

		return expiredTime;
	}

	public void setExpiredTime(long expiredTime) {

		this.expiredTime = expiredTime;
	}
}
