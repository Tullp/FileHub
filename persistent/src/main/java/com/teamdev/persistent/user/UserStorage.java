package com.teamdev.persistent.user;

import com.teamdev.persistent.Storage;

import java.util.Optional;

/**
 * Addition to {@linkplain Storage base storage interface},
 * that contains methods for interactions with {@linkplain UserRecord users records}.
 */
public interface UserStorage extends Storage<UserRecord, UserId> {

	Optional<UserRecord> findByLoginAndPassword(String login, String password);
}
