package com.teamdev.persistent.user;

import com.teamdev.persistent.Storage;

import java.util.Optional;

/**
 * Addition to {@linkplain Storage base storage interface},
 * that contains methods for interactions with {@linkplain UserSessionRecord sessions records}.
 */
public interface UserSessionStorage extends Storage<UserSessionRecord, UserSessionId> {

	Optional<UserSessionRecord> findByToken(String token);
}
