package com.teamdev.persistent.user;

import com.google.common.base.Preconditions;
import com.teamdev.persistent.AbstractInMemoryStorage;

import java.util.Optional;

/**
 * Implementation of {@link UserStorage}, that based on {@link AbstractInMemoryStorage}.<br>
 * Represents of memory storage for {@linkplain UserRecord user records}.
 */
public class UserInMemoryStorage extends AbstractInMemoryStorage<UserRecord, UserId> implements UserStorage {

	@Override
	public Optional<UserRecord> findByLoginAndPassword(String login, String password) {

		Preconditions.checkNotNull(login);

		Preconditions.checkNotNull(password);

		return records().stream()
			.filter(user -> user.login().equals(login) && user.password().equals(password))
			.findFirst();
	}
}
