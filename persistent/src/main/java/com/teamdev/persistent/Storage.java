package com.teamdev.persistent;

import java.util.Optional;

/**
 * Interface for storage representations, that contains CRUD operations.
 *
 * @param <D> type of {@linkplain DataRecord data record}.
 * @param <I> type of {@linkplain DataRecordId data record's id}.
 */
public interface Storage<D extends DataRecord<?>, I extends DataRecordId<?>> {

	void insert(D dataRecord) throws DuplicatedDataRecordIdException;

	Optional<D> findById(I dataRecordId);

	void update(D dataRecord);

	boolean delete(I dataRecordId);
}
