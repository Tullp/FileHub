package com.teamdev.persistent;

import com.google.common.base.Preconditions;

/**
 * Base abstraction, that represents records with data in {@linkplain Storage storage}.
 *
 * @param <I> type of data record {@linkplain DataRecordId identifier}.
 */
public class DataRecord<I extends DataRecordId<?>> {

	private final I id;

	protected DataRecord(I id) {

		this.id = Preconditions.checkNotNull(id);
	}

	public I id() {

		return id;
	}
}
