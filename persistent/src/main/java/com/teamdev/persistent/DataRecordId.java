package com.teamdev.persistent;

import com.google.common.base.Preconditions;

import java.util.StringJoiner;

/**
 * Base abstraction, that represents identifier for {@linkplain DataRecord data record}.
 *
 * @param <T> type of identifier.
 */
public class DataRecordId<T> {

	private final T identifier;

	protected DataRecordId(T identifier) {

		this.identifier = Preconditions.checkNotNull(identifier);
	}

	public T identifier() {

		return identifier;
	}

	@Override
	public String toString() {

		return new StringJoiner(", ", this.getClass().getSimpleName() + " {", " }")
			.add(String.format("identifier = %s", identifier))
			.toString();
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) {

			return true;
		}

		if (o == null || getClass() != o.getClass()) {

			return false;
		}

		DataRecordId<?> that = (DataRecordId<?>) o;

		return identifier.equals(that.identifier);
	}

	@Override
	public int hashCode() {

		return identifier.hashCode();
	}
}
